import {CounterControl} from "../CounterControl/CounterControl";
import {CounterResult} from "../CounterResult/CounterResult";
import "./style.css"

export const MainComponent = () => {
    return (
      <div className={'main-wrap'}>
        <div className={'side-control'}>
            <CounterControl/>
        </div>
        <div className={'side-result'}>
            <CounterResult/>
        </div>
      </div>
    );
}
