import {useSelector} from "react-redux";
import {counterReducer} from "../redux/Counter/reducer";

export const CounterResult = () => {
    const {count, lastSet} = useSelector(store => ({
        count: store.counterReducer.count,
        lastSet: store.counterReducer.lastSet,
    }));
    return (
        <div>
            <h3>Результат счетчика:</h3>
            <b>Кол-во</b>: {count}
            <br/>
            <b>Последнее действие</b>: {lastSet}
        </div>
    );
}
