
const initialState = {
    color: '',
}

export const colorReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET': return {...state, ...{color: action.val}}
        default: return {...state}  // TODO: спросить, а нужно ли новый экземпляр возвращать
    }
}
