import {counterTypes} from "./type";

const initialState = {
    count: 0,
    lastSet: '----',
}

export const counterReducer = (state = initialState, action) => {
    switch (action.type) {
        case counterTypes.INC: return {...state, ...{count: state.count + 1, lastSet: 'Инкремент'}}
        case counterTypes.DEC: return {...state, ...{count: state.count - 1, lastSet: 'Декремент'}}
        case counterTypes.ADD10: return {...state, ...{count: state.count + 10, lastSet: 'Добавка 10'}}
        case counterTypes.SET:
            let correctVal = +action.val;
            if (isNaN(correctVal)) correctVal = 0;
            return {...state, ...{count: correctVal, lastSet: 'Ручная установка'}}
        default: return {...state}  // TODO: спросить, а нужно ли новый экземпляр возвращать
    }
}
