import {counterTypes} from "./type";

/* Экшен-криейторы для редюсера Counter.
Вообще говоря, не обязательно было их упаковывать в объект. Просто так они сгруппированы, и когда из вызываешь,
то всегда четко понимаешь, от какого редьюсера экшен вызван. */
export const counterActions = {
    inc: ()=>({type: counterTypes.INC}),
    dec: ()=>({type: counterTypes.DEC}),
    add10: ()=>({type: counterTypes.ADD10}),
    set: (val)=>({type: counterTypes.SET, val: val}),
}
