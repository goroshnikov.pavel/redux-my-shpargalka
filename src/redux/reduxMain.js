import {counterReducer} from './Counter/reducer';
import {colorReducer} from './Colors/reducer';
import {combineReducers, legacy_createStore} from "redux";

/* Сокращенная запись, фишка самого JS-а. работает и в браузерном джсе)
{val1, val2}  тождественно: {val1: val1, val2: val2}  */
const rootReducer = combineReducers({
    counterReducer,
    colorReducer,
});

// Уже устарел, и на смену пришел tolkit. Прийдется с ним разрбраться
export const store = legacy_createStore(rootReducer);
