import {useState} from "react";
import {useDispatch} from "react-redux";
import {counterActions} from "../redux/Counter/action.";

export const CounterControl = () => {
    const [customVal, setCustomVal] = useState(0);
    const dispatch = useDispatch();
    return (
        <div>
            <div>
                Операции с счетчиком:
            </div>
            <div>
                <button onClick={()=>dispatch(counterActions.dec())}>-</button>
                <button onClick={()=>dispatch(counterActions.inc())}>+</button>
                <button onClick={()=>dispatch(counterActions.add10())}>+10</button>
            </div>
            <div>
                <button onClick={()=>dispatch(counterActions.set(+customVal))}>Установить: </button>
                <input
                    type={'text'}
                    value={customVal}
                    onChange={(event)=>setCustomVal(event.target.value)}
                />
            </div>
        </div>
    );
}
